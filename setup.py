from setuptools import setup, find_packages
import jobs_afunnilator

requirements = [
    "plotly",
    "texthero",
    "pandas",
    "requests",
    "gensim",
    "numpy",
    "sklearn~=0.0",
    "scikit-learn~=0.23",
    "beautifulsoup4",
    "kneed",
    "tensorflow_hub",
    "tensorflow",
]

setup(
    name="Jobs Afunnilator",
    version=str(jobs_afunnilator.__VERSION__),
    packages=find_packages(),
    description="Simple scraper of job websites",
    long_description=str(
        "Simple scraper of job websites with clustering visualisation options."
    ),
    url="https://gitlab.com/beneklins/jobs_afunnilator",
    license="GPLv3",
    install_requires=requirements,
    entry_points={
        "console_scripts": ["jobs_afunnilator=jobs_afunnilator.jobs_afunnnilator:main"],
    },
)
