================
Jobs Afunnilator
================

Scrapes job postings from Indeed and other websites. Also, allows the
visualisation of postings via various clustering and dimensionality techniques
to make it easier [citation needed] to find similar jobs by title or
description.

Supported websites
==================

* Indeed

  * Canada
  * France
  * Switzerland

Acknowledgements and licence
============================

This software is based on the work by Peter Szemraj
(https://github.com/pszemraj/scrape-viz-jobs), which in turn is derived from
the work of Chris Lovejoy (https://github.com/chris-lovejoy/job-scraper).

This software is licensed under the GPL v3 licence (or, at your option, any
later version). See LICENCE for more details.
