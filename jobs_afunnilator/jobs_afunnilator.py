#!/usr/bin/env python3

"""
Jobs Afunnilator: a simple jobs website scraper.

Copyright © 2022 Igor Benek-Lins <physics@ibeneklins.com>.
Copyright © 2021 Peter Szemraj.

Licence
=======
For the licence, see the LICENCE file.

"""

import sys
import os
import argparse
import yaml

import pandas as pd

import scrapers
import scrapers.indeed.main as indeed
from utils import csv_database as db

LINKS_COLS = ["url_job_board"]
COLS_TO_DROP = [
    "url",
    "url_job_board",
    "description_raw",
    "date_listed",
    "website",
    "favourite",
    "status",
]
LONG_TEXT_COLS = ["description_raw", "description", "summary"]
VERBOSE_COLS = LINKS_COLS + LONG_TEXT_COLS


def job_scraper(
    search_params,
    database,
    debug,
):
    """Extract information of job postings.

    This function extracts information of all new job postings associated to
    the job title and location specified. It creates a CSV file in addition to
    returning a DataFrame.

    Parameters
    ----------
    search_params: dict
        Dictionary containing all custom search parameters.
    database: pd.DataFrame
        The current job postings database.
    debug: dict
        Dictionary containing debug information.

    Returns
    -------
    pd.DataFrame:
        Dataframe with information on the jobs found. Data columns are as
        follows:

        ===============  ======  ==========================================
        name             dtype   definition
        ===============  ======  ==========================================
        title            str     Title of the position
        company          str     Company name
        date_listed      str     List date
        website          str     Website where the ad was found
        location         str     Job location
        pay              str     Job remuneration information
        summary          str     Summary of the position
        url_job_board    str     Link to the ad in the job board/website
        url              str     Resolved link to the original posting
        description      str     Complete description of the position
        type             str     Position type (full time, internship, ...)
        remote           bool    Remote work?
        search_keywords  str     Search keywords associated to the entry
        status           str     Status field (external user input)
        favourite        bool    Favourite flag (external user input)
        ===============  ======  ==========================================

    """
    website = search_params.get("website")
    keywords = search_params.get("keywords")
    if website in ("indeed", "indeed-english"):
        if search_params["indeed-country"] == "ca":
            country_info = ("ca", "Canada")
        if search_params["indeed-country"] == "ch":
            country_info = ("ch", "Switzerland")
        if search_params["indeed-country"] == "fr":
            country_info = ("fr", "France")
        indeed_country_code = country_info[0]
        website_label = f"{website}-{indeed_country_code}"
        search_params["website_label"] = website_label
    if website == "indeed":
        sp_search = indeed.indeed_load_jobs(
            search_params,
            country_info,
        )
        # TODO: Add check to verify if the webpage was properly downloaded.
        job_soup = sp_search.get("job_soup")
        jobs_list, num_listings = indeed.indeed_job_information(
            job_soup,
            website_label,
            keywords,
            indeed_country_code,
            database,
            debug,
        )
    elif website == "indeed-english":
        sp_search = indeed.indeed_load_jobs(
            search_params, country_info, run_default=True
        )
        job_soup = sp_search.get("job_soup")
        jobs_list, num_listings = indeed.indeed_job_information(
            job_soup,
            website_label,
            keywords,
            indeed_country_code,
            database,
            debug,
        )

    jobs_df = pd.DataFrame(jobs_list).set_index("url")
    # Parse the database to fill with some default values.
    jobs_df = db.prepare(jobs_df, search_params)

    print(f"{num_listings} job postings retrieved from {website_label}.")

    return jobs_df


parser = argparse.ArgumentParser(description="Jobs Afunnilator")
parser.add_argument(
    "-d",
    "--debug",
    action="store_true",
    dest="debug",
    default=False,
    help="Debug mode. Default: false",
)
parser.add_argument(
    "--only-cluster",
    action="store_true",
    dest="only_cluster",
    default=False,
    help=str(
        "Do not scrape for jobs. Only generates cluster plots."
        " This option implies `--visualisation`. Default: false"
    ),
)
parser.add_argument(
    "--semantic-similarity",
    action="store",
    dest="semantic_similarity",
    choices=["google_use", "gensim_w2v"],
    default="google_use",
    help=str("Model for semantic similarity. Default: `google_use`"),
)
parser.add_argument(
    "--cluster",
    action="store",
    dest="cluster",
    choices=["pca", "tsne"],
    default="",
    help=str(
        "Visualise cluster plots using the given dimensionality reduction method."
    ),
)
parser.add_argument(
    "--cluster-search-keyword",
    action="store",
    dest="cluster_search_keyword",
    choices=["no", "group-all", "specific-keyword"],
    default="no",
    help=str(
        "Whether to group cluster plots by search keyword."
        " With `specific-keyword`, the user will interactively choose which keywords to use."
        " Default: `no`."
    ),
)
parser.add_argument(
    "--cluster-similarity-column",
    action="store",
    dest="cluster_columns",
    nargs="+",
    choices=["all", "description", "summary", "title"],
    default="description",
    help=str(
        "Data field(s) to consider when calculating semantic similarity."
        " Default: `description`"
    ),
)
parser.add_argument(
    "-c",
    "--config",
    dest="config",
    type=str,
    action="store",
    default="config.yaml",
    help="Configuration file to read from. Default: `config.yaml`",
)

args = parser.parse_args()
with open(args.config, "r") as config_file:
    config = yaml.safe_load(config_file)

if __name__ == "__main__":
    output_dir = os.path.expanduser(config["general"]["base_directory"])
    cluster_columns = args.cluster_columns
    db_filename = config["general"]["database_filename"]
    output_db_file = os.path.join(output_dir, db_filename)
    output_dir_visualisation = os.path.join(
        output_dir, config["general"]["visualisation_output_dir"]
    )
    debug_info = dict(
        status=args.debug, dir=os.path.join(output_dir, "debug")
    )
    semantic_similarity_model = args.semantic_similarity

    # TODO: Move these checks to a function.
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    if not os.path.exists(output_dir_visualisation):
        os.makedirs(output_dir_visualisation)
    if not os.path.exists(debug_info["dir"]):
        os.makedirs(debug_info["dir"])
    if not os.path.isfile(output_db_file):
        print("Empty database.")

    jobs_db = db.load(
        output_dir, db_filename, scrapers.INFO_FIELDS, index_col="url"
    )
    searches_params = config["searches"]
    searches = []

    if not args.only_cluster:
        for search in searches_params:
            searches.append(job_scraper(search, jobs_db, debug_info))
        new_job_postings = db.add_postings(*searches)
        jobs_db = db.add_postings(jobs_db, new_job_postings)
        db.save(jobs_db, output_db_file)

    if args.cluster or args.only_cluster:
        if len(jobs_db) == 0:
            sys.exit()
        if args.cluster_search_keyword in ("group-all", "specific-keywords"):
            jobs_db_group = jobs_db.groupby(jobs_db["search_keywords"])
            visualisation_keywords = jobs_db["search_keywords"].unique()
        if args.cluster_search_keyword == "specific-keywords":
            from pick import pick

            pick_title = str(
                "Choose which search queries to visualise."
                "\t[press <SPACE> to mark, <ENTER> to continue]: "
            )
            visualisation_keywords_pick = pick(
                visualisation_keywords,
                pick_title,
                multiselect=True,
                min_selection_count=1,
            )
            visualisation_keywords = [
                value for value, idx in visualisation_keywords_pick
            ]

        import data_visualisation.plot as plot

        if args.cluster_search_keyword == "no":
            plot.generate_plots(
                jobs_db,
                "",
                semantic_similarity_model,
                args.cluster,
                cluster_columns,
                COLS_TO_DROP,
                output_dir_visualisation,
            )
        elif args.cluster_search_keyword in ("group-all", "specific-keyword"):
            for query in visualisation_keywords:
                plot.generate_plots(
                    jobs_db_group.get_group(query),
                    query,
                    semantic_similarity_model,
                    args.cluster,
                    cluster_columns,
                    COLS_TO_DROP,
                    output_dir_visualisation,
                )
