#!/usr/bin/env python3

import os
from pathlib import Path
import pandas as pd
from datetime import datetime
from scrapers import INFO_FIELDS_TO_PARSE_NEW_LINES


def load(
    dir_path,
    filename,
    columns,
    index_col="url",
):
    path = os.path.join(dir_path, filename)
    if check_db_path(dir_path, filename):
        df = pd.read_csv(path).set_index(index_col)
    else:
        df = init(columns, index_col)
    return df


def prepare(
    df,
    search_params,
    cols_to_replace_newline=INFO_FIELDS_TO_PARSE_NEW_LINES,
):
    """Parse a DataFrame to fit it to our standard."""
    # Escape new-line characters to avoid entries across multiple lines.
    for rm_nl_col in cols_to_replace_newline:
        df[rm_nl_col] = df[rm_nl_col].astype(str).str.replace("\n", r"\n")
    # Add website, as well as default `favourite` and `status` values.
    if "website_label" in search_params.keys():
        df["website"] = search_params.get("website_label")
    else:
        df["website"] = search_params.get("website")
    df["favourite"] = False
    df["status"] = ""
    return df


def save(df, path_to_db):
    if os.path.isfile(path_to_db):
        day_time_tag = datetime.strftime(datetime.now(), "%Y-%m-%d_%Hh%Mmin")
        backup_path = Path(path_to_db).with_suffix(f".{day_time_tag}.csv.bak")
        os.rename(path_to_db, backup_path)
    df.to_csv(path_to_db)


def check_db_path(dir_path, filename, permission=0o700):
    """Verify if the path to the database and the database file exist.

    Return `False` if the database does not exist, `True` otherwise.
    """
    path = os.path.join(dir_path, filename)
    if os.path.exists(dir_path):
        if not os.path.isfile(path):
            return False
    else:
        os.makedirs(dir_path, permission)
        return False
    return True


def add_postings(*df_list):
    df_dedup = pd.concat(df_list)
    return df_dedup.drop_duplicates()


def init(columns, index_col):
    return pd.DataFrame(columns=columns).set_index(index_col)
