#!/usr/bin/env python3

from datetime import datetime


def text_first_chars(text, num=40):
    """Return the first `n` chars of `text`."""
    # Convert to string
    text = str(text)
    # Create a string if we obtain list.
    if isinstance(text, list):
        text = " ".join(text)
    # Filter the string.
    if len(text) >= num:
        short_text = text[:num]
        return short_text + "…"
    return text


def df_postprocess(i_df, fields_to_clean=["title", "summary", "description"]):
    """Apply Texthero cleaning to the columns defined in `fields_to_clean`."""
    import texthero as hero

    for field in fields_to_clean:
        i_df[field] = hero.clean(i_df[field])

    return i_df
