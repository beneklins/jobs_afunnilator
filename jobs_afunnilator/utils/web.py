#!/usr/bin/env python3

import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromiumService
from selenium.webdriver.chrome.options import ChromiumOptions
from webdriver_manager.chrome import ChromeDriverManager
from webdriver_manager.core.utils import ChromeType
from selenium_stealth import stealth


def url2soup(url, headers="", engine="selenium"):
    # TODO: “Turn” this function into a class.
    if engine == "selenium":
        # Prevent Selenium detection.
        options = ChromiumOptions()
        options.headless = True
        options.add_argument("start-maximized")
        options.add_experimental_option(
            "excludeSwitches", ["enable-automation"]
        )
        options.add_experimental_option("useAutomationExtension", False)
        driver = webdriver.Chrome(
            service=ChromiumService(
                ChromeDriverManager(chrome_type=ChromeType.CHROMIUM).install()
            ),
            options=options,
        )
        stealth(
            driver,
            languages=["en-US", "en"],
            vendor="Google Inc.",
            platform="Win32",
            webgl_vendor="Intel Inc.",
            renderer="Intel Iris OpenGL Engine",
            fix_hairline=True,
        )
        driver.get(url)
        soup = BeautifulSoup(driver.page_source, "lxml")
    elif engine == "requests":
        page = requests.get(url, headers=headers)
        soup = BeautifulSoup(page.content, "lxml")
    return soup


def resolve_url(input_url):
    # Similar to using
    #   curl -Ls -w %{url_effective} -o /dev/null
    # in the command line.
    # TODO: Improve error handling. We should treat “Name or service not
    # known” differently from “unable to get local issuer certificate” and so
    # on.
    try:
        head = requests.head(input_url, allow_redirects=True)
        return head.url
    except:
        print(f"An error has occurred whilst trying to resolve “{input_url}”")
        return input_url
