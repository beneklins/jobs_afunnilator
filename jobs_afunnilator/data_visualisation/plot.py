#!/usr/bin/env python3

from data_visualisation import data_visualisation as data_vis
from utils import utils


def generate_plots(
    df_jobs,
    query,
    sem_sim_model,
    dim_reduction,
    fields,
    cols_to_drop,
    output_dir,
):

    df_processed = utils.df_postprocess(df_jobs)

    # Load model.
    if sem_sim_model == "google_use":
        my_embeddings = data_vis.load_google_use()
    elif sem_sim_model == "gensim_w2v":
        w2v_model = data_vis.load_gensim_word2vec()

    vis_df = df_processed.copy()
    vis_df.reset_index(inplace=True)
    # TODO: Invert the behaviour to select only the columns to keep.
    vis_df.drop(columns=cols_to_drop, inplace=True)

    if sem_sim_model == "google_use":
        for col in fields:
            data_vis.google_use(
                vis_df,
                col,
                my_embeddings,
                output_dir,
                save_plot=True,
                show_text=False,
                query_name=f"{query}",
                vis_type=dim_reduction,
            )
    elif sem_sim_model == "gensim_w2v":
        for col in fields:
            data_vis.word2vec(
                vis_df,
                col,
                w2v_model,
                output_dir,
                save_plot=True,
                h=720,
                query_name=f"{query}",
            )
