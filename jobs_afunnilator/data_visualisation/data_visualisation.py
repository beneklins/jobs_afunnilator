#!/usr/bin/env python3

import os
import time
from datetime import date
from datetime import datetime

from utils import utils

import numpy as np
import pandas as pd
import gensim.downloader as api
import plotly.express as px
import texthero as hero
import tensorflow_hub as hub
from kneed import KneeLocator
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler


def optimal_num_clustas(
    input_matrix,
    d_title,
    output_dir,
    top_end=11,
    show_plot=False,
    write_image=False,
):
    # Given 'input_matrix' as a pandas series containing a list / vector in
    # each row, find the optimal number of k_means clusters to cluster them
    # using the elbow method.

    # 'top_end' is the max number of clusters. If having issues, look at the
    # plot and adjust accordingly.
    scaler = StandardScaler()
    # Texthero input data structure is weird. We stole the below if/else from
    # the source code behind TH kmeans function
    # https://github.com/jbesomi/texthero/blob/master/texthero/representation.py
    if isinstance(input_matrix, pd.DataFrame):
        # Fixes weird issues parsing a Texthero edited text pd series.
        input_matrix_coo = input_matrix.sparse.to_coo()
        input_matrix_for_vectorization = input_matrix_coo.astype("float64")
    else:
        input_matrix_for_vectorization = list(input_matrix)

    scaled_features = scaler.fit_transform(input_matrix_for_vectorization)
    kmeans_kwargs = {
        "init": "random",
        "n_init": 30,
        "max_iter": 300,
        "random_state": 42,
    }
    # A list holds the SSE values for each k.
    sse = []
    for k in range(1, top_end):
        kmeans = KMeans(n_clusters=k, **kmeans_kwargs)
        kmeans.fit(scaled_features)
        sse.append(kmeans.inertia_)

    # Plot to illustrate (viewing it is optional).
    title_k = "Optimal k-means for" + d_title
    kmeans_opt_df = pd.DataFrame(
        list(zip(range(1, top_end), sse)),
        columns=["Number of clusters", "SSE"],
    )
    f_k = px.line(
        kmeans_opt_df, x="Number of clusters", y="SSE", title=title_k
    )
    # Find optimum.
    kl = KneeLocator(
        range(1, top_end), sse, curve="convex", direction="decreasing"
    )
    onk = kl.elbow

    if onk is None:
        print(f"Warning - {d_title} has no solution for optimal k-means")
        print(f"Returning # of clusters as max allowed ({top_end})")
        return top_end

    if onk == top_end:
        print(
            f"Warning - {d_title} opt. # equals max value searched ({top_end})"
        )

    print(f"\nFor {d_title}: opt. # of k-means clusters is {onk} \n")
    # Add vertical line to Plotly.
    f_k.add_vline(x=onk)

    if show_plot:
        f_k.show()

    if write_image:
        f_k.write_image(os.path.join(output_dir, f"{title_k}.png"))

    return onk


def vis_job_data(vis_df, text_col_name, output_dir, save_plot=False, h=720):
    today = date.today()
    td_str = today.strftime("%Y-%m-%d")

    vis_df["tfidf"] = vis_df[text_col_name].pipe(hero.clean).pipe(hero.tfidf)

    vis_df["kmeans"] = (
        vis_df["tfidf"].pipe(hero.kmeans, n_clusters=5).astype(str)
    )

    vis_df["pca"] = vis_df["tfidf"].pipe(hero.pca)

    hv_list = list(vis_df.columns)
    hv_list.remove("tfidf")
    hv_list.remove("pca")
    hv_list.remove("summary")
    hv_list.remove("description")

    plot_title = f"{td_str} companies by {text_col_name} data"

    # Reformat data so do not have to use built-in plotting.
    df_split_pca = pd.DataFrame(
        vis_df["pca"].to_list(), columns=["pca_x", "pca_y"]
    )
    # Drop original PCA column.
    vis_df.drop(columns="pca", inplace=True)
    # Merge dataframes.
    vis_df = pd.concat([vis_df, df_split_pca], axis=1)

    # Plot pca data. Texthero also features other ways to reduce dimensions
    # besides pca. See documentation.
    w = int(h * (4 / 3))
    fig_s = px.scatter(
        vis_df,
        x="pca_x",
        y="pca_y",
        color="kmeans",
        hover_data=hv_list,
        title=plot_title,
        height=h,
        width=w,
        template="plotly_dark",
    )
    fig_s.show()

    if save_plot:
        fig_s.write_html(
            f"{output_dir}/{plot_title}.html", include_plotlyjs=True
        )

    print("Plot generated - ", datetime.now())


def load_gensim_word2vec(wvmodel="word2vec-google-news-300", verbose=False):
    # Another option is the smaller set: `word2vec-ruscorpora-300`
    loaded_model = api.load(wvmodel)

    print("Loaded data for word2vec - ", datetime.now())

    if verbose:
        # For more info or bug fixing.
        wrdvecs = pd.DataFrame(
            loaded_model.vectors, index=loaded_model.key_to_index
        )
        print("Created dataframe from word2vec data- ", datetime.now())
        print("Dimensions of the dataframe: \n", wrdvecs.shape)

    print("Testing gensim model...")
    test_string = "computer"
    vector = loaded_model.wv[test_string]

    print(f"The shape of string {test_string} is: \n {vector.shape}")
    print("Test complete - ", datetime.now())

    return loaded_model


def get_vector_freetext(input_text, model, verbose=0, cutoff=2):
    # Iterate through all words in the input text generate the word2vec vector for
    # that word, then take the mean of those. Only words with length 3 or greater
    # are considered. **Note that if a word is not in the Google News dataset, it
    # is just skipped. If you think the graphs / representations are not good
    # enough, you should check to ensure that a large amount of words (or fraction
    # rather) are not being skipped).** The verbose parameter helps with that.

    # verbose = 1 shows you how many words were skipped.
    # verbose = 2 tells you each individual skipped word and ^
    # 'cutoff' removes all words with length N or less from the rep. vector.

    lower_it = input_text.lower()
    # Yes, this is an assumption.
    input_words = lower_it.split(" ")
    usable_words = [word for word in input_words if len(word) > cutoff]

    list_of_vectors = []
    num_words_total = len(usable_words)
    num_excluded = 0

    for word in usable_words:
        try:
            this_vector = model.wv[word]
            list_of_vectors.append(this_vector)
        except:
            num_excluded += 1
            if verbose == 2:
                print(
                    f"\nThe word/term `{word}' is not in the model vocabulary."
                )
                print("Excluding from representative vector.")

    rep_vec = np.mean(list_of_vectors, axis=0)

    if verbose > 0:
        print(
            "Computed representative vector."
            f" Excluded {num_excluded} words out of {num_words_total}."
        )

    return rep_vec


def word2vec(
    vis_df,
    text_col_name,
    model,
    output_dir,
    save_plot=False,
    h=720,
    query_name="",
    show_text=False,
):
    today = date.today()
    td_str = today.strftime("%Y-%m-%d")

    # Compute word2vec average vector for each row of text.
    vis_df["avg_vec"] = vis_df[text_col_name].apply(
        get_vector_freetext, args=(model,)
    )

    # Get optimal number of kmeans. Limit maximum to 15 for interpretability.
    max_clusters = 15
    if len(vis_df["avg_vec"]) < max_clusters:
        max_clusters = len(vis_df["avg_vec"])

    kmeans_numC = optimal_num_clustas(
        vis_df["avg_vec"],
        "word2vec-" + query_name,
        output_dir,
        top_end=max_clusters,
    )

    # Complete k-means clustering + PCA dimensionality reduction with average_vec.
    if kmeans_numC is None:
        kmeans_numC = 5

    vis_df["kmeans"] = (
        vis_df["avg_vec"]
        .pipe(
            hero.kmeans,
            n_clusters=kmeans_numC,
            algorithm="elkan",
            random_state=42,
            n_init=30,
        )
        .astype(str)
    )
    # Texthero has other algorithms to reduce dimensions besides PCA. See
    # documentation.
    vis_df["pca"] = vis_df["avg_vec"].pipe(hero.pca)

    # generate list of column names for hover_data
    hv_list = list(vis_df.columns)
    hv_list.remove("avg_vec")
    hv_list.remove("pca")
    if "tfidf" in hv_list:
        hv_list.remove("tfidf")
    if "summary" in hv_list:
        hv_list.remove("summary")
    if "description" in hv_list:
        hv_list.remove("description")

    # Reformat data so we do not have to use Txthero built-in plotting.
    df_split_pca = pd.DataFrame(
        vis_df["pca"].to_list(), columns=["pca_x", "pca_y"]
    )
    # Drop original PCA column.
    vis_df.drop(columns="pca", inplace=True)
    # Merge dataframes.
    vis_df = pd.concat([vis_df, df_split_pca], axis=1)

    # Set up plot parameters (width, title, text).
    w = int(h * (4 / 3))

    if len(query_name) > 0:
        # User provided query_name, so include it.
        plot_title = f"{td_str}: jobs by {text_col_name} - word2vec + PCA | {query_name}"
    else:
        plot_title = f"{td_str}: jobs by {text_col_name} - word2vec + PCA"

    if show_text:
        # Adds company names to the plot if you want.
        vis_df["companies_abbrev"] = vis_df["companies"].apply(
            utils.text_first_chars, num=15
        )
        graph_text_label = "companies_abbrev"
    else:
        graph_text_label = None

    # Plot dimension-reduced data.
    fig_w2v = px.scatter(
        vis_df,
        x="pca_x",
        y="pca_y",
        color="kmeans",
        hover_data=hv_list,
        title=plot_title,
        height=h,
        width=w,
        template="plotly_dark",
        text=graph_text_label,
    )
    fig_w2v.show()

    # Save if requested.
    if save_plot:
        # Saves the HTML file. Auto-saving as a static image is difficult, so
        # just click on the interactive plot it generates.
        fig_w2v.write_html(
            f"{output_dir}/{plot_title}_{query_name}_{text_col_name}.html",
            include_plotlyjs=True,
        )

    print("Plot generated - ", datetime.now())


def load_google_use():
    st = time.time()
    print("Loading Google USE embeddings...")
    embed = hub.load("https://tfhub.dev/google/universal-sentence-encoder/4")
    rt = (time.time() - st) / 60
    print(f"Loaded Google USE embeddings in {round(rt, 2)} minutes")

    return embed


def google_use(
    vis_df,
    text_col_name,
    use_embedding,
    output_dir,
    save_plot=False,
    h=720,
    query_name="",
    show_text=False,
    vis_type="tsne",
):
    today = date.today()
    td_str = today.strftime("%Y-%m-%d")

    # Generate embeddings for Google USE. `use_embedding` MUST be passed in.
    # Create list from arrays.
    embeddings = use_embedding(vis_df[text_col_name])
    use = np.array(embeddings).tolist()
    # Add lists as dataframe columns.
    vis_df["use_vec"] = use

    # Get optimal number of kmeans. Limit maximum to 15 for interpretability.
    max_clusters = 15
    if len(vis_df["use_vec"]) < max_clusters:
        max_clusters = len(vis_df["use_vec"])

    kmeans_numC = optimal_num_clustas(
        vis_df["use_vec"],
        "google_USE-" + query_name,
        output_dir,
        top_end=max_clusters,
    )

    # Complete k-means clustering + PCA dimensionality reduction with use_vec.
    if kmeans_numC is None:
        kmeans_numC = 5

    vis_df["kmeans"] = (
        vis_df["use_vec"]
        .pipe(
            hero.kmeans,
            n_clusters=kmeans_numC,
            algorithm="elkan",
            random_state=42,
            n_init=30,
        )
        .astype(str)
    )

    # Use the vector for dimensionality reduction.
    if vis_type.lower() == "tsne":
        vis_df["TSNE"] = vis_df["use_vec"].pipe(hero.tsne, random_state=42)
    else:
        vis_df["pca"] = vis_df["use_vec"].pipe(hero.pca)

    # Generate list of column names for hover_data in the HTML plot.
    hv_list = list(vis_df.columns)
    hv_list.remove("use_vec")

    if "tfidf" in hv_list:
        hv_list.remove("tfidf")
    if "pca" in hv_list:
        hv_list.remove("pca")
    if "TSNE" in hv_list:
        hv_list.remove("TSNE")
    if "summary" in hv_list:
        hv_list.remove("summary")
    if "description" in hv_list:
        hv_list.remove("description")

    # Reformat data so we do not have to use Texthero built-in plotting.
    if vis_type.lower() == "tsne":
        # TSNE reformat
        df_split_tsne = pd.DataFrame(
            vis_df["TSNE"].to_list(), columns=["tsne_x", "tsne_y"]
        )
        # Drop original PCA column.
        vis_df.drop(columns="TSNE", inplace=True)
        # Merge dataframes.
        vis_df = pd.concat([vis_df, df_split_tsne], axis=1)
    else:
        # PCA reformat
        df_split_pca = pd.DataFrame(
            vis_df["pca"].to_list(), columns=["pca_x", "pca_y"]
        )
        # Drop original PCA column.
        vis_df.drop(columns="pca", inplace=True)
        # Merge dataframes.
        vis_df = pd.concat([vis_df, df_split_pca], axis=1)

    # Set up plot parameters (width, title, text)
    w = int(h * (4 / 3))

    # If user provided `query_name`, include it.
    plot_title = f"{td_str} jobs by {text_col_name} - Google USE {vis_type}"
    if len(query_name) > 0:
        plot_title += f" | {query_name}"
    if show_text:
        # Add company names to the plot if you want.
        vis_df["companies_abbrev"] = vis_df["companies"].apply(
            utils.text_first_chars, num=15
        )
        graph_text_label = "companies_abbrev"
    else:
        graph_text_label = None

    # Setup labels (decides pca or tsne).
    if vis_type.lower() == "tsne":
        plt_coords = ["tsne_x", "tsne_y"]
    else:
        plt_coords = ["pca_x", "pca_y"]

    # TODO: Determine strategy to deal with missing data.
    #       Should we just keep the most general columns from the start?
    # vis_df.dropna(inplace=True)

    # Plot dimension-reduced data.
    fig_use = px.scatter(
        vis_df,
        x=plt_coords[0],
        y=plt_coords[1],
        color="kmeans",
        hover_data=hv_list,
        title=plot_title,
        height=h,
        width=w,
        template="plotly_dark",
        text=graph_text_label,
    )
    fig_use.show()

    # Save if requested.
    if save_plot:
        # Saves the HTML file. Auto-saving as a static image is difficult so
        # just click on the interactive plot it generates.
        plot_filename = f"{plot_title}{query_name}_{text_col_name}.html"
        fig_use.write_html(
            os.path.join(output_dir, plot_filename),
            include_plotlyjs=True,
        )

    print("Plot generated - ", datetime.now())
