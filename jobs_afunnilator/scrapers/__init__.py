USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; rv:100.0) Gecko/20100101 Firefox/100.0"
HEADERS = {
    "User-Agent": USER_AGENT,
    "Accept-Encoding": "identity",
    "Accept": "text/xml,application/xml,application/xhtml+xml,text/html;q=0.9",
    "Connection": "Keep-Alive",
    "Accept-Language": "en-us,en;q=0.5",
    "Accept-Charset": "ISO-8859-1,utf-8;q=0.7,*;q=0.7",
    "Keep-Alive": "300",
}
INFO_FIELDS = [
    "title",
    "company",
    "date_listed",
    "website",
    "location",
    "pay",
    "summary",
    "url",
    "url_job_board",
    "description_raw",
    "description",
    "type",
    "remote",
    "search_keywords",
    "favourite",
    "status",
]
INFO_FIELDS_TO_PARSE_NEW_LINES = ["description", "description_raw"]
