#!/usr/bin/env python3

import os
import urllib
import requests
import re
from bs4 import BeautifulSoup
import scrapers
import utils.web as web
from scrapers.indeed.common import extract_javascript_data, parse_date
from tqdm import tqdm
from datetime import datetime


def indeed_load_jobs(
    search_params,
    country,
    run_default=False,
    ad_limit=5,
    debug={"status": True},
):
    """Scrape job posting published on Indeed.

    The following versions of Indeed were tested:
    - Switzerland
    - France
    - Canada

    Indeed {Switzerland, France, Canada} appear to have unique pages for job
    postings in English at
    https://{2-letter country code}.indeed.com/{country name}-English-Jobs"
    """
    country_code, country_name = country
    base_url = f"https://{country_code}.indeed.com/jobs?"
    base_english_only_url = f"{base_url}q={country_name}+English&"
    job_query = search_params.get("keywords")
    job_ad_age = search_params.get("job_ad_age")
    job_type = search_params.get("job_type")
    language = search_params.get("language")
    if run_default:
        get_vars = {
            "jt": job_type,
            "fromage": job_ad_age,
            "limit": ad_limit,
            "sort": "date",
        }
        # If values are not specified, remove them from the search query.
        if job_type is None:
            del get_vars["jt"]
        if job_ad_age is None:
            del get_vars["fromage"]
        url = f"{base_english_only_url}{urllib.parse.urlencode(get_vars)}"
        if debug["status"]:
            print(f"Parsing {url}")
        soup = web.url2soup(url)
        job_soup = soup.find(id="resultsCol")
    else:
        get_vars = {
            "q": job_query,
            "jt": job_type,
            "lang": language,
            "fromage": job_ad_age,
            "limit": ad_limit,
            "sort": "date",
        }
        # If values are not specified, remove them from the search query.
        if job_query is None:
            del get_vars["q"]
        if job_type is None:
            del get_vars["jt"]
        if language is None:
            del get_vars["lang"]
        if job_ad_age is None:
            del get_vars["fromage"]
        url = f"{base_url}{urllib.parse.urlencode(get_vars)}"
        if debug["status"]:
            print(f"Parsing {url}")
        soup = web.url2soup(url)
        job_soup = soup.find(
            id=["resultsCol", "mosaic-jobcards", "jobsearch-Main"]
        )

    # Return the job soup.
    soup_results = {"job_soup": job_soup, "query_URL": url}
    return soup_results


def indeed_job_information(
    job_soup,
    indeed_label,
    search_keywords,
    country_code,
    database,
    debug,
):
    """Extract information from Indeed job posting."""
    existing_job_urls = database["url_job_board"].values
    existing_resolved_job_urls = database.index.values
    job_elems = job_soup.find_all("div", class_="result")

    if debug["status"]:
        timestamp = datetime.strftime(datetime.now(), "%Y-%m-%d_%Hh%Mmin")
        output_debug_file = os.path.join(
            debug["dir"],
            f"job_elements_{indeed_label}_{timestamp}.txt",
        )
        with open(output_debug_file, "w") as f:
            # Save elements to text file for debugging purposes.
            print(job_elems, file=f)

    cols = scrapers.INFO_FIELDS
    extracted_info = list()
    jobs_list = dict()

    title = []
    company = []
    date = []
    location = []
    pay = []
    summary = []
    url = []
    url_job_board = []
    description_raw = []
    description = []
    job_type = []
    remote = []

    for job_elem in tqdm(job_elems, indeed_label):
        # URL.
        current_job_url = indeed_link(job_elem, country_code)
        # Skip if this job (via the job post URL) is already in the database.
        if current_job_url in existing_job_urls:
            continue

        # Get resolved URL pointing to the original job posting.
        current_resolved_job_url = indeed_original_job_url(
            job_soup, current_job_url
        )
        # Skip if this job (via its resolved URL) is already in the database.
        if current_resolved_job_url in existing_resolved_job_urls:
            continue

        # Get full job ad webpage.
        job_soup = web.url2soup(
            f"http://{current_job_url}", headers=scrapers.HEADERS
        )

        # Check if webpage is not a checkpoint.
        if "Checking if the site connection is secure" in str(job_soup):
            print(f"Manual intervention required. Skipping {current_job_url}")
            continue

        url.append(current_resolved_job_url)
        url_job_board.append(current_job_url)
        # Summary.
        summary.append(indeed_summary(job_elem))
        # Get information from full job ad webpage.
        job_info = extract_javascript_data(job_soup)
        date.append(parse_date(job_info["date"]))
        title.append(job_info["title"])
        company.append(job_info["company"])
        location.append(job_info["location"])
        job_type.append(job_info["type"])
        pay.append(job_info["pay"])
        description_raw.append(job_info["description_raw"])
        description.append(job_info["description"])
        remote.append(job_info["remote"])

    # Number of new jobs found ().
    num_new_job_postings = len(url_job_board)
    # Create filled lists for the remaining fields.
    website = [None] * num_new_job_postings
    keywords = [search_keywords] * num_new_job_postings
    favourite = [None] * num_new_job_postings
    status = [None] * num_new_job_postings

    extracted_info.append(title)
    extracted_info.append(company)
    extracted_info.append(date)
    extracted_info.append(website)
    extracted_info.append(location)
    extracted_info.append(pay)
    extracted_info.append(summary)
    extracted_info.append(url)
    extracted_info.append(url_job_board)
    extracted_info.append(description_raw)
    extracted_info.append(description)
    extracted_info.append(job_type)
    extracted_info.append(remote)
    extracted_info.append(keywords)
    extracted_info.append(favourite)
    extracted_info.append(status)

    for j in range(len(cols)):
        jobs_list[cols[j]] = extracted_info[j]

    return jobs_list, num_new_job_postings


def indeed_title_html(job_elem):
    """Extract title of job posting from the HTML source."""
    title_elem = job_elem.select_one("span[title]").text
    if title_elem is not None:
        title = title_elem.strip()
    else:
        title = "no title"
    return title


def indeed_company_html(job_elem):
    """Extract company name offering the position from the HTML source."""
    company_elem = job_elem.find("span", class_="companyName")
    company = company_elem.text.strip()
    return company


def indeed_location_html(job_elem):
    """Extract location information from the HTML source."""
    return ""


def indeed_pay_html(job_elem):
    """Extract pay information from the HTML source."""
    return ""


def indeed_link(job_elem, country_code):
    """Extract link to the job posting from the HTML source."""
    link_job_post = job_elem.find("a")["href"]
    link_job_post = f"{country_code}.indeed.com{link_job_post}"
    return link_job_post


def indeed_description_html(job_soup):
    """Extract description of the position from the HTML source."""
    job_description_elem = job_soup.find(id="jobDescriptionText")
    if job_description_elem is None:
        job_description = "[no description]"
    else:
        job_description = " ".join(map(str, job_description_elem.contents))
        job_description = re.sub("\n", r"<br/>", job_description)
    return job_description


def indeed_original_job_url(job_soup, job_ad_url):
    """Get URL to the original job posting from the HTML source."""
    link_div = job_soup.find("div", id="originalJobLinkContainer")
    if link_div is None:
        return job_ad_url
    else:
        url = link_div.find("a")["href"]
        resolved_url = web.resolve_url(url)
        return resolved_url


def indeed_date(job_elem):
    """Extract date information about when the job posting was published."""
    date_elem = job_elem.find("span", class_="date")
    date = date_elem.text.strip()
    return date


def indeed_summary(job_elem):
    """Get summary of the position from the HTML source."""
    summary_elem = job_elem.find("div", class_="job-snippet")
    summary = summary_elem.text.strip()
    summary = re.sub("\n", " ", summary)
    return summary


def indeed_job_type_html(job_elem):
    """Get the job type (full time, internship, ...) from the HTML source."""
    return ""


def indeed_remoteness_html(job_elem):
    """Get remoteness information about the job from the HTML source."""
    return ""
