#!/usr/bin/env python3

import re
from bs4 import BeautifulSoup
import json
from dict_deep import deep_get
from dateparser import parse as dateparser_parse


def extract_javascript_data(soup):
    """Extract job information from an appropriate JavaScript code block.

    Indeed ads have most of their information readily available in a
    JavaScript variable containing JSON-encoded data. First, we need to
    extract the JSON data from the variable holding it. Then, the information
    of interest can be found in the data by following distinct paths in the
    resulting dictionary.

    """
    # Variable/attribute holding the data.
    JS_VARIABLE = "window._initialData"
    # “Paths” to the desired information fields found in the JSON data. Each
    # “level” is separated by `DICT_PATH_SEP`.
    DICT_PATH_SEP = "."
    INFORMATION_JS_DICT = dict(
        company="jobInfoWrapperModel.jobInfoModel.jobInfoHeaderModel.companyName",
        date="jobMetadataFooterModel.age",
        description_raw="jobInfoWrapperModel.jobInfoModel.sanitizedJobDescription.content",
        location="jobInfoWrapperModel.jobInfoModel.jobInfoHeaderModel.formattedLocation",
        pay="jobInfoWrapperModel.jobInfoModel.jobMetadataHeaderModel.salaryInfo.salaryText",
        remote="jobInfoWrapperModel.jobInfoModel.jobInfoHeaderModel.remoteWorkModel.text",
        title="jobInfoWrapperModel.jobInfoModel.jobInfoHeaderModel.jobTitle",
        type="jobInfoWrapperModel.jobInfoModel.jobMetadataHeaderModel.jobType",
    )
    # Dictionary to store parsed data.
    job_data = dict()

    # Get the script block containing our element of interest.
    pattern = re.compile(JS_VARIABLE)
    javascript_block = soup.find("script", text=pattern)

    if javascript_block is None:
        # Populate output  dictionary with `None`s.
        for key, value in INFORMATION_JS_DICT.items():
            job_data[key] = None
        return job_data

    # Get the contents of the script, which can be parsed as JSON data.
    javascript_code = javascript_block.string.strip()
    raw_javascript_data = re.search(
        r"window._initialData=({.*});", javascript_code
    ).group(1)
    raw_job_data = json.loads(raw_javascript_data)
    # Access the deep dictionary entries of interest.
    for key, value in INFORMATION_JS_DICT.items():
        job_data[key] = deep_get(raw_job_data, value, sep=DICT_PATH_SEP)
    # Parse raw description to get it in plain text.
    job_data["description"] = BeautifulSoup(
        job_data["description_raw"], "lxml"
    ).text.strip()

    return job_data


def parse_date(string):
    """Extract date from Indeed's human-readable job post date field.

    There are 4 possible formats of date strings:
    1. Exactly how many days ago the job was posted:
       - [de] `vor 11 Tagen`
       - [fr] `il y a 3 jours`
       - [en] `10 days ago`
    2. If the job was posted in the last dozen of hours, it is flagged as
       "today" or "yesterday". Examples (today):
       - [de] `Heute`
       - [fr] `Accord's`
       - [en] `Today`
    3. When posted a few hours ago:
       - [de] `Gerade geschaltet`
       - [fr] `Publié à l'instant`
       - [en] `Just posted`
    4. If the job was posted more than a month ago, a general text is displayed:
       - [de] `vor 30+ Tagen`
       - [fr] `il y a 30+ jours`
       - [en] `30+ days ago`

    Only formats 1 and 2 are parseable using `dateparser`. Format 3 is just
    "today". Format 4 poses a problem as it does not give us an specific date.
    However, since we assume searches to occur frequently and with date limits
    set to less than a month, we can safely label them as being posted 30 days
    ago without too many consequences.

    """
    REGEX_LAST_2_DAYS = re.compile(r"gerade|à l'instant|just", re.IGNORECASE)

    # Return an empty string in the case of None.
    if string is None:
        return ""

    # Format 4.
    if "+" in string:
        date = dateparser_parse("30 days ago")
    # Format 3.
    elif REGEX_LAST_2_DAYS.findall(string):
        date = dateparser_parse("today")
    # Format 1 and 2 should be the remaining ones.
    else:
        date = dateparser_parse(string)

    # If we cannot parse the date, return the original string.
    if not date:
        return string

    date_string = date.strftime("%Y-%m-%d")

    return date_string
