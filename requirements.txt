PyYAML
beautifulsoup4
dateparser
dict_deep
gensim
kneed
lxml
numpy
pandas
pick
plotly
requests
webdriver-manager
selenium
selenium_stealth
# Until Texthero, https://github.com/jbesomi/texthero/issues/218, is fixed,
# let us use scikit-learn 0.23. Version 0.24.2 has a deprecation warning for
# `precompute_distances` and `n_jobs`, 1.0 removes them.
scikit-learn~=0.23
tensorflow
tensorflow_hub
texthero
